# README #

### What is this repository for? ###

Este proyecto está basado en el ejemplo de: https://www.codetutorial.io/nodejs-socket-io-and-jhonny-five-to-control-arduino/
Se basa en encender/apagar un led conectado a un Arduino (en este caso el led 13 que viene incorporado en la placa) mediante el protocolo de comunicación Johnny-five desde Node y Angular.

### How do I get set up? ###

Para utilizarlo, tendrás que clonar este repositorio y ubicandote en la carpeta del mismo (ruta.../led_node), ejecutar mediante linea de comandos el fichero *server.js*: **node server.js**.

Quizás te de algun error si no tienes instalados de manera global las dependencias necesarias como Johnny-five, en tal caso reinstalalas con el gestor *npm*.

En un explorador indicar en la URL: localhost:3000

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Si tienes alguna duda de cómo llevarlo a cabo me comentas, yo también tuve mis quebraderos para hacerlo funcionar. :)