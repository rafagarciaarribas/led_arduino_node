// server.js
var express        = require('express');  
var app            = express();  
var httpServer = require("http").createServer(app);  
var five = require("johnny-five");  
 
var io=require('socket.io')(httpServer);

var port = 3000; 
//var led;
var arduino={
  temperatura: Number,
  led: {type: String, default: "apagado"} 
};

app.use(express.static(__dirname + '/public'));

//ruta de tipo GET
app.get('/', function(req, res) {  //la funcion de callback del GET
        res.sendFile(__dirname + '/public/index.html');
});

app.get('/temperatura',function(req,res){
  
  res.send(200, arduino.temperatura)
});

httpServer.listen(port);  
console.log('Server available at http://localhost:' + port);  

//Arduino board connection
var board = new five.Board();  
board.on("ready", function() { 
    // code to be executed when the board is ready 
    console.log('Arduino connected');
    arduino.led = new five.Led(13);
});

//Socket connection handler
io.on('connection', function (socket) {  
        console.log(socket.id);
  
        socket.on('led:on', function (data) {
           arduino.led.on();
           console.log('LED ON RECEIVED');
        });

        socket.on('led:off', function (data) {
            arduino.led.off();
            console.log('LED OFF RECEIVED');
            
        });
     
  });

console.log('Waiting for connection');

//Sensor de temperatura 
board.on("ready", function() {
  var temp = new five.Thermometer({
    pin: "A0",
    controller: "LM35"
  });

  temp.on("change", function() {
    console.log(this.celsius + "°C", this.fahrenheit + "°F");
    arduino.temperatura=this.celsius;
  });

//Cuando reciba el mensaje connection, envie por consola que alguien se ha conectado
//es decir, cuando se inicie una conexion a la web del servidor
io.on('connection', function(socket){ 
  setInterval(function(){
    socket.emit('messages', "La última temperatura registrada es: " + arduino.temperatura + "°C");
  }, 1000);
});
     
});